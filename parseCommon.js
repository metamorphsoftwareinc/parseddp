/* globals module, require, requirejs */
'use strict';

    var commonmark = require('commonmark'),
        xmljsonconverter = require('./xmljsonconverter.js');

    function endsWith(input, suffix) {
        return suffix.length <= input.length && input.lastIndexOf(suffix) === (input.length - suffix.length);
    }

    var ddp = {

        setIcon: function (component, info, zip) {
            var icon = (component.ResourceDependency || []).filter(function (dependency) {
                return endsWith(dependency['@Path'].toLowerCase(), 'icon.png');
            })[0];
            if (icon && icon['@Path']) {
                var path = icon['@Path'].replace('\\', '/');
                if (zip.file(path)) {
                    info.icon = 'data:image/png;base64,' + zip.file(path).asNodeBuffer().toString('base64');
                }
            }
        },

        setDatasheet: function (self, component, info, zip) {
            var dependency = (component.ResourceDependency || []).filter(function (dep) {
                return endsWith(dep['@Path'].toLowerCase(), '.pdf');
            })[0];
            if (dependency && dependency['@Path']) {
                var path = dependency['@Path'].replace('\\', '/');
                if (zip.file(path)) {
                    info.datasheet = self.getDatasheetUrl(path);
                }
            }
        },

        setMarkdown: function (component, info, zip) {
            var dependency = (component.ResourceDependency || []).filter(function (dep) {
                return endsWith(dep['@Path'].toLowerCase(), '.md') || endsWith(dep['@Name'].toLowerCase(), '.mdown');
            })[0];
            if (dependency && dependency['@Path']) {
                var path = dependency['@Path'].replace('\\', '/');
                if (zip.file(path)) {
                    var reader = new commonmark.Parser();
                    var writer = new commonmark.HtmlRenderer();
                    var parsed = reader.parse(zip.file(path).asText());
                    info.documentation = writer.render(parsed);
                }
            }
        },

        setProperties: function (component, info) {
            info.properties = {};
            (component.Property || []).forEach(function (prop) {
                var propInfo = info.properties[prop['@Name']] = {};
                if (prop.Value && prop.Value.ValueExpression && prop.Value.ValueExpression.Value) {
                    propInfo.value = prop.Value.ValueExpression.Value['#text'];
                } else if (prop.Value && prop.Value.ValueExpression && prop.Value.ValueExpression.AssignedValue && prop.Value.ValueExpression.AssignedValue.Value) {
                    propInfo.value = prop.Value.ValueExpression.AssignedValue.Value['#text'];
                } else if (prop.Value && prop.Value.ValueExpression && prop.Value.ValueExpression.Default && prop.Value.ValueExpression.Default.Value) {
                    propInfo.value = prop.Value.ValueExpression.Default.Value['#text'];
                }
            });
        },

        setConnectors: function (component, info) {
            // connectors: [{name: "EPM0_N", type: "", description: ""}, {name: "EPM2_N", type: "", description: ""},…]
            info.connectors = [];
            (component.Connector || []).forEach(function (connector) {
                info.connectors.push({name: connector['@Name'], type: connector['@Definition'], description: connector['@Notes']});
            });
        },

        convertDdpToJson: function (xmlString) {
            var converter = new xmljsonconverter.Xml2json({
                skipWSText: true,
                arrayElements: {
                    Property: true,
                    Connector: true,
                    DomainModel: true,
                    Role: true,
                    Formula: true,
                    Operand: true,
                    Port: true,
                    ResourceDependency: true
                }
            });

            return converter.convertFromString(xmlString);

        }
    };

    module.exports = ddp;
