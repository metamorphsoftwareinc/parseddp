/* globals module, require, requirejs */
'use strict';

    var q = require('q'),
        ddp = require("./parseCommon.js"),
        JSZip = require('jszip');

    var ParseAdm = function (id) {
        this.id = id;
    };

    ParseAdm.prototype.getfile = function getfile(path) {
        var self = this;
        return this.getZip()
            .then(function (ddpXmlZip) {
                var ddpZip = new JSZip(ddpXmlZip);

                return ddpZip.file(path).asNodeBuffer();
            });
    };

    ParseAdm.prototype.parse = function ParseAdm() {
        var self = this;
        return this.getZip()
            .then(function (ddpXmlZip) {
                var ddpZip = new JSZip(ddpXmlZip),
                    filterZipList = function (ar) {
                        return ar.filter(function (entry) {
                            return entry.name.indexOf('__MACOSX') !== 0;
                        });
                    },
                    ddpXml = filterZipList(ddpZip.file(/\.adm$/));

                if (ddpXml.length !== 1) {
                    return q.reject('zip must contain exactly 1 adm');
                }

                var ddpJson = ddp.convertDdpToJson(ddpXml[0].asText()),
                    component = ddpJson.Design,
                    ddpInfo = {};

                if ( component.ResourceDependency !== undefined ) {

                    ddp.setIcon(component, ddpInfo, ddpZip);

                    ddp.setDatasheet(self, component, ddpInfo, ddpZip);

                    ddp.setMarkdown(component, ddpInfo, ddpZip);

                    ddp.setProperties(component, ddpInfo);
                }

                ddpInfo.name = component['@Name'] || self.id;
                ddpInfo.classification = component.Classifications === undefined ? "Unclassified" : component.Classifications['#text'];

                return ddpInfo;
            });
    };

    ParseAdm.prototype.getZip = function () {
        throw new Error('must be overridden. return a q promise');
    };

    module.exports = ParseAdm;
