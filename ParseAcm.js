/* globals module, require, requirejs */
'use strict';

    var q = require('q'),
        ddp = require("./parseCommon.js"),
        JSZip = require('jszip');

    var ParseAcm = function (id) {
        this.id = id;
    };

    ParseAcm.prototype.getfile = function getfile(path) {
        var self = this;
        return this.getZip()
            .then(function (ddpXmlZip) {
                var ddpZip = new JSZip(ddpXmlZip);

                return ddpZip.file(path).asNodeBuffer();
            });
    };

    ParseAcm.prototype.parse = function ParseAcm() {
        var self = this;
        return this.getZip()
            .then(function (ddpXmlZip) {
                var ddpZip = new JSZip(ddpXmlZip),
                    filterZipList = function (ar) {
                        return ar.filter(function (entry) {
                            return entry.name.indexOf('__MACOSX') !== 0;
                        });
                    },
                    ddpXml = filterZipList(ddpZip.file(/\.acm$/));

                if (ddpXml.length !== 1) {
                    return q.reject('zip must contain exactly 1 acm');
                }

                var ddpJson = ddp.convertDdpToJson(ddpXml[0].asText()),
                    component = ddpJson.Component,
                    ddpInfo = {};

                ddp.setIcon(component, ddpInfo, ddpZip);

                ddp.setDatasheet(self, component, ddpInfo, ddpZip);

                ddp.setMarkdown(component, ddpInfo, ddpZip);

                ddp.setProperties(component, ddpInfo);

                ddp.setConnectors(component, ddpInfo);

                ddpInfo.name = ddpJson.Component['@Name'] || self.id;
                ddpInfo.classification = ddpJson.Component.Classifications['#text'];

                return ddpInfo;
            });
    };

    ParseAcm.prototype.getZip = function () {
        throw new Error('must be overridden. return a q promise');
    };

    module.exports = ParseAcm;
